#!/usr/bin/env bash


echo "Update packages"
cp -r /vagrant/provision/linuxConfiguration/etc/apt/* /etc/apt
apt-get update -q

echo "Installing packages - extra"
apt-get install sudo wget curl git-core  vim elinks -y

wget https://www.dotdeb.org/dotdeb.gpg
sudo apt-key add dotdeb.gpg

echo "Installing packages - apache"
apt-get install apache2  -y
echo "Installing packages - php"
apt-get install php-common libapache2-mod-php php-dev php-cli php-curl \
        libapache2-mod-php php-dev php-cli php-curl php-mbstring \
        php-gd php-mcrypt php-mysql php-xml php-json  -y --force-yes


if [ ! -f  /usr/local/bin/composer ]; then
    echo "Get composer"
    curl -sS https://getcomposer.org/installer | php  
    mv composer.phar /usr/local/bin/composer  
    chmod a+x /usr/local/bin/composer 
fi
if ! [ -L /var/www ]; then
    echo "Check apache2"
    cp -r /vagrant/provision/linuxConfiguration/etc/apache2/* /etc/apache2
    cp -r /vagrant/provision/linuxConfiguration/etc/php5/* /etc/php5
    a2enmod rewrite 
    service apache2 restart
fi
if [ ! -f /usr/sbin/mysqld ]; then
    echo "Installing packages - mysql"
    DEBIAN_FRONTEND=noninteractive apt-get  -y install mysql-server
    mysqladmin -u root password root
fi
if ls -c1 /var/lib/mysql | grep beblog; then
     echo "Database exists nothing to do"
else
     echo "Create database"
     mysqladmin -u root -proot create beblog
fi
if [ ! -f /vagrant/vendor ]; then
    echo "Get vendors via composer"
    cd /vagrant
    composer install 
fi
